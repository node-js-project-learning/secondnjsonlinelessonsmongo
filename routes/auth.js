

const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');

const models = require('../models');
const { model } = require('../models/post');
const { use } = require('browser-sync');

// POST is authorized
router.post('/register', (req, res) => {

    const login = req.body.login;
    const password = req.body.password;
    const passwordConfirm = req.body.passwordConfirm;
    let errorText = '';
    let fields = ['login'];

    console.log('firsterror', !login || !password || !passwordConfirm);


    if (!login || !password || !passwordConfirm) {
        fields = [];
        if (!login) fields.push('login')
        if (!password) fields.push('password')
        if (!passwordConfirm) fields.push('passwordConfirm')

        errorText = 'All filds must be fild';


    } else if (!/^[a-zA-Z0-9]+$/.test(login)) {

        errorText = 'Only latin letters and numbers';

    } else if (login.length < 3 || login.length > 16) {

        errorText = 'Login length must be from 3 to 16 symbols';

    } else if (password !== passwordConfirm) {

        errorText = 'Passwords is not equal';
        fields = ['password', 'passwordConfirm'];

    } else if (password.length < 5) {
        errorText = 'Minimum Passwords length must be 5 symbols';

    } else {

        // Mangus сработал хорошо , но на видео уроке не сработал , потому делаю этот код
        models.User.findOne({
            login
        }).then(user => {
            // Так как это в промисе , это код отрабатывает после того как завершилась процедура
            if (user !== null) {
                res.json({
                    ok: false,
                    error: 'login already in use',
                    fields
                });

            } else {
                console.log('will create new', login);

                bcrypt.hash(password, null, null, (err, hash) => {
                    models.User.create({
                        login,
                        password: hash
                    }).then(user => {
                        console.log('user was created', user);

                        res.session.userId = user.id;
                        res.session.userLogin = user.login;
                        
                        res.json({
                            ok: true
                        });
                    }).catch(err => {
                        errorText = err;
                    });
                });
            }

        }).catch(err => {
            console.log(err);
            res.json({
                ok: false,
                error: 'Try again later',
            });
        });



    }

    if (errorText) {
        res.json({
            ok: false,
            error: errorText,
            fields
        });
    }


});


// POST is register(login)
router.post('/login', (req, res) => {

    const login = req.body.login;
    const password = req.body.password;
    let fields = ['login']['password'];

    if (!login || !password) {
        fields = [];
        if (!login) fields.push('login')
        if (!password) fields.push('password')

        res.json({
            ok: false,
            error: 'All filds must be fild',
            fields
        });



    } else {

        models.User.findOne({
            login
        }).then(user => {
            // Так как это в промисе , это код отрабатывает после того как завершилась процедура
            if (user === null) {
                res.json({
                    ok: false,
                    error: 'There is no such login or password',
                    fields
                })
            } else {
                console.log('result',user);
                bcrypt.compare(password, user.password, function (err, result) {
                    
                    if (!result) {
                        // Not found
                        res.json({
                            ok: false,
                            error: 'There is no such login or password',
                            fields
                        })

                    } else {
                        // Found
                        
                        console.log(user.id, user.login);
                        
                        req.session.userId = user.id;
                        req.session.userLogin = user.login;

                        res.json({
                            ok: true
                        });
                    }

                });
            }
        }).catch(err => {
            console.log(err);
            res.json({
                ok: false,
                error: 'Try again later',
            });
        });
    }
});


// GET for logout
router.get('/logout', (req, res) => {
    if (req.session) {
      // delete session object
      req.session.destroy(() => {
        res.redirect('/');
      });
    } else {
      res.redirect('/');
    }
  });

module.exports = router;