// npm run dev - стартануть nodemon
//'mongodb://localhost:27017/TestNodeJS'

const dotenv = require('dotenv');
const path = require('path');

const root = path.join.bind(this, __dirname);
dotenv.config({ path: root('.env') });

module.exports = {
    PORT: process.env.PORT || 3001,
    MONGO_URL: process.env.MONGO_URL,
    SESSION_SECRET: process.env.SESSION_SECRET, 
    IS_PRODUCTUION: process.env.NODE_ENV === 'production'
};

