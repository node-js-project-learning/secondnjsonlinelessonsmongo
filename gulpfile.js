const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const nodemon = require('nodemon');
//const cssnano = require('gulp-cssnano');
//const browsersync = require('browser-sync');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');


function css() {
  return gulp
    .src('./dev/scss/**/*.scss')
    //.pipe(plumber())
    .pipe(sass())
    .pipe(
      autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
        cascade: true
      })
    )
    //.pipe(cssnano()) 
    .pipe(gulp.dest('./public/stylesheets'))
}


function scripts() {
  return gulp
    .src(['dev/js/auth.js', 'dev/js/post.js', 'node_modules/medium-editor/dist/js/medium-editor.min.js'])
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/javascripts'))
};

gulp.task('scripts', scripts);


function serveR() {
    nodemon({
      script: 'app.js',
      watch: ["app.js", "gulpfile.js", 'routes/*', 'public/*', 'public/*/**', 'views/*/**'],
      ext: '.js,.ejs,.json '
    }).on('restart', () => {
      gulp.src('app.js')
    });
    gulp.watch('dev/scss/**/*.scss', css);
    gulp.watch('dev/js/*.js', scripts);
  }

module.exports.default = gulp.series(serveR, css, scripts);