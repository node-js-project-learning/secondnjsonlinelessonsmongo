
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const staticAsset = require('static-asset');
const mongoose = require('mongoose');
const session = require('express-session');

const MongoStore = require('connect-mongo')(session);
//const { error } = require('console');

const config = require('./config');
const routes = require('./routes');
//const { unique } = require('jquery');

// data base connection

mongoose.Promise = global.Promise;
//mongoose.set('debug', ture);

console.log('3', config.MONGO_URL);

mongoose.connection
    .on('error', error => reject(error))
    .on('close', () => console.log('Database connection closed.'))
    .once('open', () => {
        const info = mongoose.connection;
        console.log(`Connection to ${info.host}:${info.port}/${info.name}`);
    });

mongoose.connect(config.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });

// express
const app = express();


// sessions
console.log('SESSION_SECRET', config.SESSION_SECRET);


//app.set('trust proxy', 1); // trust first proxy
app.use(
    session({
        secret: config.SESSION_SECRET,
        resave: true,
        saveUninitialized: false,
        store: new MongoStore({
            mongooseConnection: mongoose.connection
        })
    })
);

// sets 
app.set('view engine', 'ejs');

// uses
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(staticAsset(path.join(__dirname + 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    '/javascripts',
    express.static(path.join(__dirname, 'node_modules', 'jquery', 'dist'))
);

// routes
app.get('/', (req, res) => {

    const id = req.session.userId;
    const login = req.session.userLogin;

    res.render('index', {
        user: {
            id,
            login
        }

    })
});

app.use('/api/auth', routes.auth);
app.use('/post', routes.post);

// catch 404
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    console.log(error.status);

    res.render('error', {
        message: error.message,
        error: error,

    });
});

// Тут передавали данные из формы в базу данных
// app.get('/', (req, res) => {
//     Post.find({}).then( posts => {
//         res.render('index', { posts: posts })
//     })
// });


app.listen(config.PORT, () =>
    console.log(`exmample app ${config.PORT}`)
);
